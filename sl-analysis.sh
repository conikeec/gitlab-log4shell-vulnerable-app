#!/bin/sh

#### Analyze code
BASEDIR=$(dirname $0)
echo "Script location: ${CI_PROJECT_DIR}"

# run analysis (Qwiet SAST) - post build 
SHIFTLEFT_SBOM_GENERATOR=2 sl analyze \
  --app "$CI_PROJECT_NAME" \
  --oss-project-dir "$CI_PROJECT_DIR" \
  --version-id "$CI_COMMIT_SHA" \
  --tag branch="$CI_COMMIT_REF_NAME" \
  --wait \
  build/libs/gitlab-log4shell-vulnerable-app-all.jar

# Check if this is running in a merge request
if [ -n "$CI_MERGE_REQUEST_IID" ]; then
  echo "Got merge request $CI_MERGE_REQUEST_IID for branch $CI_COMMIT_REF_NAME"

  # Run check-analysis and save report to /tmp/check-analysis.md (references shiftleft.yml at root of repo for build rules)
  sl check-analysis \
    --app "$CI_PROJECT_NAME" \
    --report \
    --report-file /tmp/check-analysis.md \
    --source "tag.branch=master" \
    --target "tag.branch=$CI_COMMIT_REF_NAME"

  # workflow step example : saving check analysis output as a MR comment 
  CHECK_ANALYSIS_OUTPUT=$(cat /tmp/check-analysis.md)
  COMMENT_BODY=$(jq -n --arg body "$CHECK_ANALYSIS_OUTPUT" '{body: $body}')
  export GITLAB_TOKEN="glpat-VAhezuh22FU2CS45SvUr"
  # Post report as merge request comment
  curl -i -XPOST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
    -H "PRIVATE-TOKEN: $MR_TOKEN" \
    -H "Content-Type: application/json" \
    -d "$COMMENT_BODY"

  # (BETA) Best fix CLI CI/CD native checks against current branch (will soon be a part of CLI)
  python3 field-integrations/shiftleft-utils/bestfix.py -a "$CI_PROJECT_NAME" -s . 
  BESTFIX_OUTPUT=$(cat ngsast-bestfix-$CI_PROJECT_NAME.html)
  BESTFIX_BODY=$(jq -n --arg body "$BESTFIX_OUTPUT" '{body: $body}')
  # Post report as merge request comment
  curl -i -XPOST "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$CI_MERGE_REQUEST_IID/notes" \
    -H "PRIVATE-TOKEN: $MR_TOKEN" \
    -H "Content-Type: application/json" \
    -d "$BESTFIX_BODY"
fi



